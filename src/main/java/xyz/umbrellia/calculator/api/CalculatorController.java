package xyz.umbrellia.calculator.api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import xyz.umbrellia.calculator.model.Calculator;
import xyz.umbrellia.calculator.service.CalculatorService;


@RequestMapping("api/v1/calculator") // API path.
@RestController // make the Controller available as a RestController.
public class CalculatorController {

    private final CalculatorService calculatorService;

    @Autowired //  Inject Service.
    public CalculatorController(CalculatorService calculatorService) {
        this.calculatorService = calculatorService;
    }

    @PostMapping // Make function act as POST.
    public int calculate(@RequestBody Calculator params) {
        return calculatorService.calculate(params);
    }
}


