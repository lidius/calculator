package xyz.umbrellia.calculator.model;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Calculator {

    // Defining the model for Person object.

    // Properties.
    private final String operation; // the operation required.
    private final  String parameters; // the name fo the object of type String.

    // Constructor.
    // Code can be generated.
    public Calculator(@JsonProperty("operation") String operation, @JsonProperty("parameters") String parameters) {
        this.operation = operation;
        this.parameters = parameters;
    }

    public String getOperation() {
        return operation;
    }

    public String getParameters() {
        return parameters;
    }
}
