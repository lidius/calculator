package xyz.umbrellia.calculator.service;
import org.springframework.stereotype.Service;
import xyz.umbrellia.calculator.model.Calculator;

@Service // Spring annotation to define a service.
public class CalculatorService {

    public int calculate(Calculator params) {
        String operation = params.getOperation();
        String parameters = params.getParameters();
        Integer result = 0;

        switch (operation) {
            case "+":
                result = 1;
                break;

            case "-":
                result = 2;
                break;

            case "*":
                result = 3;
                break;

            case "/":
                result = 4;
                break;

        }

        return result;
    }
}

